import numpy as np,cv2
plane3d = np.array([[250, 250, 2000], [250, -250, 2000],
                   [-250, 250, 2000], [-250, -250, 2000]])
focus = 6
pixel_per_mm = 1000 / 4  # 4um*4um
plane2d = np.empty((0, 2)).astype(int)
for point in plane3d:
    t = np.array([[focus, 0, 0], [0, focus, 0], [0, 0, 1]])@point / point[2]
    t = np.array([[pixel_per_mm, 0, 1281/2],
                 [0, pixel_per_mm, 1025/2], [0, 0, 1]])@t
    plane2d = np.append(plane2d, np.array([[t[0], t[1]]], dtype="int"), axis=0)

rect = np.zeros((1024, 1280, 3), np.uint8)
cv2.rectangle(rect, plane2d[0], plane2d[3], (255, 0, 0), -1, 8)
cv2.imshow("Imaging model", rect)
cv2.waitKey()
