# Learning-RoboMaster
This is a repo for the learing of the RoboMaster in the field of OpenCV.
<br>
So, I will put the finished task of learing to this repo.
<br>
In short, hope we will do a good job in the RoboMaster competition.
<br>
## Progress
|Lesson|Topic|Status|
|------|--------|--------:|
|Lesson 1|RoboMaster<br>Git|Finished|
|Lesson 2|Python basics<br>Numpy<br>OpenCV Introduction|Finished|
|Lesson 3|Camera imaging model<br>Linux basics|Finished|
|Lesson 4|Rigid body transformation model<br>Cmake|Finished|
|Lesson 5|Idea and process of target recognition in openCV|N/A|