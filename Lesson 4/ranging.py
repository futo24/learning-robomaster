import numpy as np,cv2,math

camera_matrix = np.array([[1.800721986092533e+03,0,6.491918068014722e+02],[0,1800.211379,4.813174653323292e+02],[0,0,1]])
distortion_coeffs = np.array([[-0.065645857311558,0.380235668804160,0,0,0]])

object_coordinate = np.float32([[-82,82,0],[-82,-82,0],[82,-82,0],[82,82,0]])
camera_coordinate = np.float32([[462,414],[463,691],[752,706],[763,425]])

image = cv2.imread('./mine.bmp')
image = cv2.GaussianBlur(image,(11,11),0)

retval, rvec, tvec = cv2.solvePnP(object_coordinate,camera_coordinate,camera_matrix,distortion_coeffs)

distance = math.sqrt(tvec[0]**2+tvec[1]**2+tvec[2]**2)/10

origin = cv2.projectPoints(np.float32([[0,0,0]]),rvec,tvec,camera_matrix,distortion_coeffs)[0][0].astype(int).tolist()[0]
x = cv2.projectPoints(np.float32([[100,0,0]]),rvec,tvec,camera_matrix,distortion_coeffs)[0][0].astype(int).tolist()[0]
y = cv2.projectPoints(np.float32([[0,100,0]]),rvec,tvec,camera_matrix,distortion_coeffs)[0][0].astype(int).tolist()[0]
z = cv2.projectPoints(np.float32([[0,0,100]]),rvec,tvec,camera_matrix,distortion_coeffs)[0][0].astype(int).tolist()[0]

cv2.line(image,origin,x,color=(0,0,255),thickness=3)
cv2.line(image,origin,y,color=(0,255,0),thickness=3)
cv2.line(image,origin,z,color=(255,0,0),thickness=3)

projected_points = cv2.projectPoints(object_coordinate,rvec,tvec,camera_matrix,distortion_coeffs)[0]
for point in projected_points:
    cv2.circle(image,(int(point[0][0]),int(point[0][1])),3,(0,0,255),-1)
cv2.putText(image, f"Distance: {str(distance)[:7]}cm", origin, cv2.FONT_HERSHEY_TRIPLEX, 1, (255, 255, 255), 1)

cv2.imshow("Distance Measurement", image)
cv2.waitKey()
