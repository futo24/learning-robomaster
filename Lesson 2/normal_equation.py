import numpy as np
X = np.concatenate((np.loadtxt("./data.csv", delimiter=",", usecols=range(0, 3)), np.ones(25).reshape(25, 1)), axis=1)
y = np.loadtxt("./data.csv", delimiter=",", usecols=(3))
theta = np.linalg.inv(X.T@X)@X.T@y
print(f"a = {theta[0]}\nb = {theta[1]}\nc = {theta[2]}\nd = {theta[3]}")

# can can need cost
d=0
for i in range(0,25):
    t=0
    for j in range(0,3):
        t=t+theta[j]*X[i][j]
    d=d+(y[i]-t-theta[3])**2
print("cost = "+str(d/50))
