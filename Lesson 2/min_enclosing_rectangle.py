import numpy as np,cv2
image = cv2.imread('./image.jpg')
hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
mask = cv2.inRange(hsv, np.array([140,180,200]), np.array([155,255,255]))
contours, _ = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
for c in contours:
    rect = cv2.boxPoints(cv2.minAreaRect(c))
    cv2.drawContours(image, [np.int0(rect)], 0, (0, 0, 255), 3)
cv2.imshow("Minimum Enclosing Rectangle", image)
cv2.waitKey()
